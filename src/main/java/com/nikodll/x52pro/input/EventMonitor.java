package com.nikodll.x52pro.input;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.naming.ConfigurationException;

import com.google.common.collect.ComparisonChain;

import net.java.games.input.Controller;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;

/**
 * JVM has to be started with
 * -Djinput.plugins=net.java.games.input.AWTEnvironmentPlugin in order to
 * monitor keyboard or mouse
 * 
 * @author nikodll
 * 
 */
public class EventMonitor implements Runnable
{
	protected static final Logger		m_Log		= LogManager.getLogManager().getLogger("default");
	private static final EventFactory	FACTORY		= EventFactory.instance;

	private boolean						isListening	= false;

	private static final int			WAIT_TIME	= 100;
	private final Set<Controller>		controllers	= Collections.synchronizedSet(new HashSet<Controller>());

	DeviceEventCallback					callback;

	public EventMonitor(DeviceEventCallback callback)
	{
		this.callback = callback;
	}

	class EventComparator implements Comparator<Event>, Serializable
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 2343158297018669760L;

		public int compare(Event o1, Event o2)
		{
			int rval = ComparisonChain.start().compare(o1.getNanos(), o2.getNanos())
					.compare(o1.getValue(), o2.getValue()).result();
			return rval;
		}
	}

	/**
	 * 
	 * @param controllerName
	 * @param index
	 *            the controller's number starting from 0, if there are multiple
	 *            controllers of the same name
	 * @throws ConfigurationException
	 */
	public boolean addController(Controller newSource) throws ConfigurationException
	{
		Objects.requireNonNull(newSource);

		if (!isListening)
		{
			isListening = true;
			Thread listenThread = new Thread(this);
			listenThread.start();
		}

		return this.controllers.add(newSource);
	}

	public boolean removeController(String controllerName)
	{
		boolean removed = false;
		for (Iterator<Controller> iterator = controllers.iterator(); iterator.hasNext();)
		{
			final Controller controller = iterator.next();
			if (controllerName.equals(controller.getName()))
			{
				iterator.remove();
				removed = true;
			}
		}

		if (controllers.size() == 0)
		{
			isListening = false;
		}

		return removed;
	}

	@Override
	public void run()
	{
		m_Log.fine("Starting listening for controllers");
		while (isListening)
		{
			for (final Controller controller : controllers)
			{
				controller.poll();
				EventQueue controllerQueue = controller.getEventQueue();
				Event event = new Event();
				while (controllerQueue.getNextEvent(event))
					callback.onEvent(FACTORY.createEvent(event));
			}
			try
			{
				Thread.sleep(WAIT_TIME);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		m_Log.fine("Stopping listening for controllers");
	}

}
