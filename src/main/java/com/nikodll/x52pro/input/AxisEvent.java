package com.nikodll.x52pro.input;

import net.java.games.input.Event;

public class AxisEvent extends InputEvent
{

	AxisEvent(Event event)
	{
		super(event);
	}

	@Override
	public Float getValue()
	{
		return event.getValue();
	}

}
