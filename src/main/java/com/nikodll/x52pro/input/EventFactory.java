package com.nikodll.x52pro.input;

import net.java.games.input.Component;
import net.java.games.input.Component.Identifier;
import net.java.games.input.Event;

public enum EventFactory
{
	instance;

	public InputEvent createEvent(Event jiEvent)
	{
		Component component = jiEvent.getComponent();
		if (null == component)
			return null;

		InputEvent result = null;
		Identifier identifier = component.getIdentifier();
		if (identifier instanceof Component.Identifier.Axis)
			result = new AxisEvent(jiEvent);
		else if (identifier instanceof Component.Identifier.Button)
			result = new ButtonEvent(jiEvent);
		else if (identifier instanceof Component.Identifier.Key)
			result = new KeyEvent(jiEvent);

		return result;
	}
}
