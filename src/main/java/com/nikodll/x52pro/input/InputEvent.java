package com.nikodll.x52pro.input;

import net.java.games.input.Component;
import net.java.games.input.Event;

public abstract class InputEvent implements RDEvent
{

	protected final Event event;

	protected InputEvent(Event event)
	{
		this.event = event;
	}
	
	public final Component getComponent() {
		return event.getComponent();
	}

	public String getSource()
	{
		return event.getComponent().getName();
	}
	
	public abstract Object getValue();

	/**
	 * Return the time the event happened, in nanoseconds.
	 * The time is relative and therefore can only be used
	 * to compare with other event times.
	 */
	public final long getNanos() {
		return event.getNanos();
	}

}
