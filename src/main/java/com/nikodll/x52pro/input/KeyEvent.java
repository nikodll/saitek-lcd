package com.nikodll.x52pro.input;

import net.java.games.input.Event;

public class KeyEvent extends InputEvent
{

	KeyEvent(Event event)
	{
		super(event);
	}

	@Override
	public Boolean getValue()
	{
		return event.getValue()>0;
	}
	
}
