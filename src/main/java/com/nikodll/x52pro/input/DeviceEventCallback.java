package com.nikodll.x52pro.input;

/**
 * @author nikodll
 *
 */
public interface DeviceEventCallback
{
	void onEvent(InputEvent id);
}
