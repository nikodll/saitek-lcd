
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.monoid.saitek;

import static us.monoid.saitek.DirectOutputLibrary.SoftButton_Down;
import static us.monoid.saitek.DirectOutputLibrary.SoftButton_Up;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Pointer;
import com.sun.jna.WString;

/**
 * Simple API to access the LCD features of a Saitek X52 Pro
 *
 * The native library needed is called DirectOutput.dll and by default is
 * installed in
 * 
 * <pre>
 * C:\Program Files\Saitek\DirectOutput
 * </pre>
 * 
 * <pre>
 * C:\Program Files (x86)\Saitek\DirectOutput
 * </pre>
 * 
 * The SDK documentation is here
 * 
 * <pre>
 * file:///C:/Program%20Files/Saitek/DirectOutput/SDK/DirectOutput.htm
 * </pre>
 * 
 * Thanks to the authors and contributors of JNA!
 *
 * @author beders@yahoo.com
 */
public class Saitek extends X52ProLibBase
{

	// too lazy to re-order bytes, this fake UUID will do
	static final String	X52_PRO	= "{06D5DA29-3BF9-204F-85FA-1E02C04FAC17}";
	List<Pointer>		devices	= new ArrayList<>();
	Pointer				x52pro;
	DirectOutputLibrary	dol;

	public Saitek(String... libraryPath)
	{
		super(libraryPath);
	}

	@Override
	protected boolean loadDriver(String... libraryPath)
	{
		// try paths individually
		for (String path : libraryPath)
		{
			if (path.length() > 0)
			{
				// System.out.println("Using jna.library.path " + path);
				System.setProperty("jna.library.path", path);
			}
			try
			{
				NativeLibrary.getInstance(DirectOutputLibrary.JNA_LIBRARY_NAME);
				dol = (DirectOutputLibrary) Native.loadLibrary(DirectOutputLibrary.JNA_LIBRARY_NAME,
						DirectOutputLibrary.class);
			} catch (UnsatisfiedLinkError err)
			{
				System.err.println("Unable to load DirectOutput.dll from " + path + " (running on "
						+ System.getProperty("os.name") + "-" + System.getProperty("os.version") + "-"
						+ System.getProperty("os.arch") + "\nContinuuing...");
				continue;
			}
			System.out.println("Loaded library from " + path);
			break;
		}

		return dol != null;
	}

	// Necessary to hold onto, otherwise the GC will collect these callbacks!
	private DirectOutputLibrary.Pfn_DirectOutput_SoftButtonChange	softButtonCallback;
	private DirectOutputLibrary.Pfn_DirectOutput_PageChange			pageCallback;

	protected boolean initDriver()
	{
		WString pluginName = new WString("Saitek");
		dol.DirectOutput_Initialize(pluginName);
		DirectOutputLibrary.HRESULT result = dol.DirectOutput_Enumerate((hDevice, pCtc) -> {
			devices.add(hDevice);
		} , null);

		for (Pointer p : devices)
		{
			Memory g = new Memory(16);

			DirectOutputLibrary.LPGUID guid = new DirectOutputLibrary.LPGUID(g.getPointer(0));
			result = dol.DirectOutput_GetDeviceType(p, guid);

			// the byte order isn't correct, but we don't care right now
			String pseudoGuid = toGuidString(guid.getPointer().getByteArray(0, 16));

			if (X52_PRO.equals(pseudoGuid))
			{
				System.out.println("Found X52 Pro! Yay!");
				x52pro = p;

				pageCallback = (Pointer hDevice, int dwPage, byte bSetActive, Pointer pCtxt) -> {
					if (bSetActive > 0)
					{
						setActivePage(dwPage);
					}
				};
				softButtonCallback = (Pointer hDevice, int dwButtons, Pointer pCtxt) -> {

					final Optional<Boolean> goUp = Optional.ofNullable(((dwButtons & SoftButton_Up) == SoftButton_Up)
							? true : ((dwButtons & SoftButton_Down) == SoftButton_Down ? false : null));

					if (goUp.isPresent())
						scrollPage(getPages().get(activePage), goUp.get());
				};

				dol.DirectOutput_RegisterPageCallback(x52pro, pageCallback, null);
				dol.DirectOutput_RegisterSoftButtonCallback(x52pro, softButtonCallback, null);

				return true;
			}
		}
		return false;
	}

	protected void registerPage(String title, int no)
	{
		WString pageTitle = new WString(title);
		DirectOutputLibrary.HRESULT result = dol.DirectOutput_AddPage(x52pro, no, pageTitle,
				DirectOutputLibrary.FLAG_SET_AS_ACTIVE);
	}

	private String toGuidString(byte[] bGuid)
	{
		final String HEXES = "0123456789ABCDEF";

		final StringBuilder hexStr = new StringBuilder(2 * bGuid.length);
		hexStr.append("{");

		for (int i = 0; i < bGuid.length; i++)
		{
			char ch1 = HEXES.charAt((bGuid[i] & 0xF0) >> 4);
			char ch2 = HEXES.charAt(bGuid[i] & 0x0F);
			hexStr.append(ch1).append(ch2);

			if ((i == 3) || (i == 5) || (i == 7) || (i == 9))
			{
				hexStr.append("-");
			}
		}

		hexStr.append("}");
		return hexStr.toString();
	}

	protected void shutdownDriver()
	{
		for (int i = 0; i < getPages().size(); i++)
		{
			dol.DirectOutput_RemovePage(x52pro, i);
		}
		dol.DirectOutput_RegisterPageCallback(x52pro, null, null);
		dol.DirectOutput_RegisterSoftButtonCallback(x52pro, null, null);
		dol.DirectOutput_Deinitialize();
	}

	protected void setText(int no, int row, String text)
	{
		WString line = new WString(text);
		DirectOutputLibrary.HRESULT result = this.dol.DirectOutput_SetString(this.x52pro, no, row, line.length(), line);
		// System.out.println("Result:" + result);
	}

	protected void setLed(int no, int ledID, int dwValue)
	{
		this.dol.DirectOutput_SetLed(this.x52pro, no, ledID, dwValue);
	}
}
