package us.monoid.saitek;

import java.io.File;

import net.octanode.x52pro.lib.X52ProLibFacade;
import net.octanode.x52pro.lib.linux.X52ProLibLinux;

/**
 * @author nikodll
 *
 */
public class X52ProLibFactory
{
	public static X52ProLibFacade createDevice(String... libPath)
	{
		final X52ProLibFacade result;
		if ('/' == File.separatorChar)
			result = new X52ProLibLinux();
		else
			result = new Saitek(libPath);

		return result;
	}
}
