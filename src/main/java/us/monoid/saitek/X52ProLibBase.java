package us.monoid.saitek;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import net.octanode.x52pro.lib.Page;
import net.octanode.x52pro.lib.X52ProLibFacade;

public abstract class X52ProLibBase implements X52ProLibFacade
{
	protected static final Logger	m_Log	= LogManager.getLogManager().getLogger("default");

	private final List<Page>		pages	= new ArrayList<>();
	protected volatile int			activePage;
	private final ExecutorService	executor;

	public X52ProLibBase(String... pathTo)
	{
		super();
		executor = Executors.newSingleThreadExecutor();
		if (!loadDriver(pathTo))
		{
			executor.shutdownNow();
			throw new IllegalArgumentException("Unable to load the driver library from the paths provided");
		} else
			executor.submit(this::init);
	}

	protected abstract void setText(int no, int row, String text);

	protected abstract void setLed(int no, int ledID, int dwValue);

	protected abstract void registerPage(String title, int no);

	protected abstract void shutdownDriver();

	protected abstract boolean loadDriver(String... pathTo);

	protected abstract boolean initDriver();

	private ExecutorService getExecutor()
	{
		return executor;
	}

	protected List<Page> getPages()
	{
		return pages;
	}

	private void init()
	{
		if (!initDriver())
		{
			m_Log.severe("No X52 Pro found! Plug it in and try again");
			getExecutor().shutdown();
		} else
			m_Log.info("Device has been successfully initialized");
	}

	@Override
	public final void updatePage(Page page)
	{
		this.getExecutor().submit(() -> {
			int no = this.getPages().indexOf(page);
			if (no == this.activePage)
			{
				for (int i = page.getTopLine(), row = 0; row < 3; i++, row++)
				{
					String text = i < page.getLines().size() ? page.getLines().get(i) : "";
					setText(no, row, text);
				}
			}
		});
	}

	@Override
	public final void updateLEDs(Page page)
	{
		int no = this.getPages().indexOf(this);
		if (!page.getLedsSet().isEmpty() && no == this.activePage)
		{
			this.getExecutor().submit(() -> {
				page.getLedsSet().stream().forEach(ledID -> {
					int dwValue = page.getLedConfig().get(ledID) ? 1 : 0;
					setLed(no, ledID, dwValue);
				});
			});
		}
	}

	@Override
	public final Page addPage(String title)
	{
		return this.addPage(title, 0);
	}

	@Override
	public final Page addPage(String title, int maxCount)
	{
		final Page page = new Page(this, title, maxCount);
		try
		{
			int no = getPages().size();
			return getExecutor().submit(() -> {
				registerPage(title, no);
				getPages().add(page);
				activePage = no;
				return page;
			}).get();
		} catch (InterruptedException ex)
		{
			Logger.getLogger(X52ProLibBase.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ExecutionException ex)
		{
			Logger.getLogger(X52ProLibBase.class.getName()).log(Level.SEVERE, null, ex);
		}
		return page;
	}

	@Override
	public final void close()
	{
		getExecutor().submit(() -> {
			// de-register callbacks, pages
			shutdownDriver();
		});
		getExecutor().shutdown();
		try
		{
			getExecutor().awaitTermination(3, TimeUnit.SECONDS);
		} catch (InterruptedException e)
		{
			m_Log.log(Level.WARNING, "Cannot shutdown driver gracefully", e);
		}
	}

	protected void setActivePage(int dwPage)
	{
		getExecutor().submit(() -> {
			activePage = dwPage;
			m_Log.fine("Page changed to:" + dwPage);
			getPages().get(dwPage).update();
		});
	}

	protected void scrollPage(Page page, boolean goUp)
	{
		getExecutor().submit(() -> {
			if (goUp)
				page.scrollUp();
			else
				page.scrollDown();
		});
	}

}