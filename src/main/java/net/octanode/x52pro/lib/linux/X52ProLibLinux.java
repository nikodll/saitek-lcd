package net.octanode.x52pro.lib.linux;

import com.sun.jna.Native;

import us.monoid.saitek.X52ProLibBase;

public class X52ProLibLinux extends X52ProLibBase
{
	private X52ProLib		lib;
	private X52LibStruct	hdl;

	public X52ProLibLinux()
	{
		super();
	}

	@Override
	protected void setText(int no, int row, String text)
	{
		lib.x52_settext(hdl, row, text, text.length());
		m_Log.info("Line " + row + " text:" + text);
	}

	@Override
	protected void setLed(int no, int ledID, int dwValue)
	{
		lib.x52_setled(hdl, ledID, dwValue);
	}

	public void setButtonsBrightness(int scale)
	{
		lib.x52_setbri(hdl, 0, scale);
	}

	public void setDisplayBrightness(int scale)
	{
		lib.x52_setbri(hdl, 1, scale);
	}

	@Override
	protected void registerPage(String title, int no)
	{
		// TODO: not yet supported on Linux
	}

	@Override
	protected void shutdownDriver()
	{
		m_Log.fine("Closing device");
		lib.x52_settext(hdl, 0, "", 0);
		lib.x52_settext(hdl, 1, "", 0);
		lib.x52_settext(hdl, 2, "", 0);
		setDisplayBrightness(0);
		setButtonsBrightness(0);
		lib.x52_close(hdl);
	}

	@Override
	protected boolean loadDriver(String... pathTo)
	{
		lib = (X52ProLib) Native.loadLibrary("x52pro", X52ProLib.class);
		return null != lib;
	}

	@Override
	protected boolean initDriver()
	{
		hdl = lib.x52_init();
		return hdl != null;
	}

}
