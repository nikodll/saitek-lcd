package net.octanode.x52pro.lib.linux;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
// import com.sun.jna.ptr.PointerByReference;

public class X52LibStruct extends Structure
{

	// public static class ByReference extends HostNameEntry implements Structure.ByReference { }
	public Pointer hdl;
	public int type;
	public boolean feat_mfd;
	public boolean feat_led;
	public boolean feat_sec;
	public boolean debug;

	@Override
	protected List<String> getFieldOrder()
	{
		return Arrays.asList(new String[] { "hdl", "type", "feat_mfd",
				"feat_led", "feat_sec", "debug" });
	}

}