package net.octanode.x52pro.lib.linux;

import com.sun.jna.Library;

public interface X52ProLib extends Library
{
	public static interface x52_type
	{
		public static final int	DEV_X52		= 0;
		public static final int	DEV_X52PRO	= 1;
		public static final int	DEV_YOKE	= 2;
	}

	/**
	 * Init function
	 * 
	 * @return handle to x52 if device opened successful
	 */
	X52LibStruct x52_init();

	void x52_debug(X52LibStruct hdl, int debug);

	x52_type x52_gettype(X52LibStruct hdl);

	/**
	 * Close function
	 * 
	 * @param hdl
	 *            handle to xte device
	 */
	void x52_close(X52LibStruct hdl);

	/**
	 * Set the text of the MFD Supported by: X52, X52PRO RETURN: 0 if successful
	 * hdl: handle to x52 device line: line of text to change text: character
	 * string of new text length: number of characters in string
	 */
	int x52_settext(X52LibStruct hdl, int line, String text, int length);

	/**
	 * Set the brightness of the MFD or the LEDs Supported by: X52, X52PRO
	 * 
	 * @param hdl
	 *            handle to x52 device
	 * @param mfd
	 *            boolean 0: change the LED brightness, 1: change the MFD
	 *            brightness
	 * @param bri
	 *            value (0-127)
	 * @return 0 if successful hdl
	 */
	int x52_setbri(X52LibStruct hdl, int mfd, int bri);

	int x52_setled(X52LibStruct hdl, int led, int on);

	int x52_settime(X52LibStruct hdl, int h24, int hour, int minute);

	int x52_setoffs(X52LibStruct hdl, int idx, int h24, int inv, int offset);

	int x52_setsecond(X52LibStruct hdl, int second);

	int x52_setdate(X52LibStruct hdl, int year, int month, int day);
}