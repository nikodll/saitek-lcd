package net.octanode.x52pro.lib;

public interface X52ProLibFacade
{
	void close();
	void updatePage(Page page);
	void updateLEDs(Page page);
	Page addPage(String title);
	Page addPage(String title, int maxCount);
}
