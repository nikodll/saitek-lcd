package net.octanode.x52pro.lib;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

/**
 * Create a page of lines. Newest lines are shown first in display. If maxCount
 * is > 0, the oldest entry will be removed if list has more than maxCount
 * elements. (Exercise for the reader to replace lines with a linked list.)
 * 
 * @author beders@yahoo.com
 *
 */
public class Page
{

	/**
	 * 
	 */
	private List<String>			lines		= Collections.synchronizedList(new ArrayList<>());
	private int						topLine		= 0;
	private final String			title;
	int								maxCount;
	private final BitSet					ledConfig	= new BitSet(20);
	private final BitSet					ledsSet		= new BitSet(20);
	private final X52ProLibFacade	facade;

	public Page(X52ProLibFacade facade, String title)
	{
		this(facade, title, 0);
	}

	public Page(X52ProLibFacade facade, String title, int maxCount)
	{
		this.facade = facade;
		this.title = title;
		this.maxCount = maxCount;
	}

	public Page addLine(String line)
	{
		if (maxCount > 0 && getLines().size() == maxCount)
		{
			getLines().remove(getLines().size() - 1);
		}
		getLines().add(0, line); // yes, array list will have to copy things,
									// but
		// at 30 items or so, we really don't care
		facade.updatePage(this);
		return this;
	}

	public synchronized void scrollUp()
	{
		setTopLine(Math.max(getTopLine() - 1, 0));
		// System.out.println("ScrollUp");
		facade.updatePage(this);
	}

	public synchronized void scrollDown()
	{
		setTopLine(Math.min(getLines().size() - 1, getTopLine() + 1));
		// System.out.println("ScrollDown");
		facade.updatePage(this);
	}

	public synchronized Page green(X52 button)
	{
		if (!button.isToggle())
		{
			setLED(button.greenID, 1);
			setLED(button.redID, 0);
		}
		return this;
	}

	public synchronized Page red(X52 button)
	{
		if (!button.isToggle())
		{
			setLED(button.greenID, 0);
			setLED(button.redID, 1);
		}
		return this;
	}

	public synchronized Page amber(X52 button)
	{
		if (!button.isToggle())
		{
			setLED(button.redID, 1);
			setLED(button.greenID, 1);
		}
		return this;
	}

	public synchronized Page off(X52 button)
	{
		if (button.isToggle())
		{
			setLED(button.id(), 0);
		} else
		{
			setLED(button.redID, 0);
			setLED(button.greenID, 0);
		}
		return this;
	}

	public synchronized Page on(X52 button)
	{
		if (button.isToggle())
		{
			setLED(button.id(), 1);
		}
		return this;
	}

	public void update()
	{
		facade.updatePage(this);
		facade.updateLEDs(this);
	}

	/**
	 * Set an LED on (1) or off (0). See the SDK documentation for the specific
	 * values or use the X52 enum
	 */
	public synchronized void setLED(int ledID, int i)
	{
		getLedsSet().set(ledID);
		getLedConfig().set(ledID, i == 1);
		facade.updateLEDs(this);
	}

	public int getTopLine()
	{
		return topLine;
	}

	public void setTopLine(int topLine)
	{
		this.topLine = topLine;
	}

	public List<String> getLines()
	{
		return lines;
	}

	public void setLines(List<String> lines)
	{
		this.lines = lines;
	}

	public BitSet getLedsSet()
	{
		return ledsSet;
	}

	public BitSet getLedConfig()
	{
		return ledConfig;
	}

}