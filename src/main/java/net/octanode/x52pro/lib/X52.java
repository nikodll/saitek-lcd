package net.octanode.x52pro.lib;

/**
 * 
 * @author beders@yahoo.com
 *
 */
public enum X52
{
	Fire(0), FireA(1, 2), FireB(3, 4), FireD(5, 6), FireE(7, 8), Toggle1_2(9, 10), Toggle3_4(11, 12), Toggle5_6(13,
			14), POV2(15, 16), Clutch(17, 18), Throttle(19);

	final int redID, greenID;

	X52(int red, int green)
	{
		this.greenID = green;
		this.redID = red;
	}

	X52(int id)
	{
		this.greenID = -1;
		this.redID = id;
	}

	boolean isToggle()
	{
		return greenID == -1;
	}

	int id()
	{
		return redID;
	}
}